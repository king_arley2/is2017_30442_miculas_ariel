#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;
my $wordHash;
my $wnCache;
my $reverseWordList;
my $counter = 0;
my @prepositions = qw(the of all which that is in or at and i you he she it we they from to most but by with his her ours our yours any here there old once between
but it other no though first some very him their were one who et la de je an more this these on for not as a be am are is been was were have had has then than whom
off if now when my it's its like your what me out up them yet upon either where etc never often always would could should do done did nothing except having already
itself under still much many less only however so something sometimes probably especially chiefly must should ought would could can may merely hardly about themselves
ourselves others among whose qui without will while thus such also before unto shall us against every even those into neither nor himself after );
my $prepositionRegex = qr/(??{join '|', @prepositions;})/i;
my $prepositionHash;
my $mafiaResult;
my $wordRegexp = qr/\b([a-zA-Z]{2,}+)(?:'[a-zA-Z]++)?\b/;
$prepositionHash->{$_} = 1 for @prepositions;
sub readFile
{
	my $filename = shift;
	
	open(my $fh, '<', $filename) or die "Could not open file '$filename' $!";
	my $txt = join "", <$fh>;
	close $fh;
	$txt;
}

sub getWordRoot
{
	my $word = shift;
	my $definition;
	return $wnCache->{$word} if defined $wnCache->{$word};
	$definition = `wn $word -over`;
	#print "running for $word\n";
	if ($definition =~ /Overview of (?:noun|verb|adj|adv) ($wordRegexp)/)
	{
		#if ($word ne $1)
		#{
		#	print "Root is $1";
		#	print ", word is $word\n";
		#}
		$word = $1;
		$wnCache->{$word} = $word;
	}
	return $word;
}

sub processInput
{
	my $filename=shift;
	my $txt = readFile($filename);
	open(my $inFile, '>', 'words.in') or die "Could not open file $!";
	my @sentences = split /(?:(?:\.|\?|;)\s)|(?:\n{2,})/, $txt;
	#@sentences = grep {length > 50} @sentences;
	my $transactionReq;
	#print Dumper @sentences;
	for my $sentence (@sentences)
	{
		$transactionReq=0;
		my @words = ($sentence =~ /$wordRegexp/g);
		for my $word (@words)
		{
			my $origWord = $word;
			$word = lc($word);
			if (not defined $prepositionHash->{$word})
			{
				$word = getWordRoot($word);
				if (not defined $wordHash->{$word})
				{
					$wordHash->{$word} = $counter;
					$reverseWordList->[$counter] = $word;
					$counter++;
				}
				$transactionReq = 1;
				print $inFile "$wordHash->{$word} ";
			}
		}
		if ($transactionReq)
		{
			print $inFile "\n";
		}
		else
		{
			# print "malformed sentence: <$sentence>\n";
		}
	}
	print "number of sentences: ", scalar@sentences, "\n";
	print "number of words: $counter\n";
	#print Dumper $reverseWordList;
	close $inFile;
}

sub processOutput
{
	return if not -f 'words.out';
	my $txt = readFile('words.out');
	my @sentences = split /\n/, $txt;
	for my $sentence (@sentences)
	{
		my @words = ($sentence =~ /\d++(?!\))/g);
		if (@words > 1)
		{
			$sentence =~ /\((\d++)\)/;
			push @{$mafiaResult}, {'words'=>[@words], 'frequency'=>$1};
		}
	}

	for my $line (sort {$b->{'frequency'}<=>$a->{'frequency'}} @{$mafiaResult})
	{
		for my $word (sort {$a<=>$b} @{$line->{'words'}})
		{
			print "$reverseWordList->[$word] ";
		}
		
		print " <$line->{'frequency'}>\n";
	}
}
sub usage
{
	"$0 filename [min support(percentage)]\n";
}
if (@ARGV < 2)
{
	print usage();
	exit();
}
processInput($ARGV[0]);
my $output = `mafia -mfi $ARGV[1] -ascii words.in words.out`;
print $output;
processOutput();
