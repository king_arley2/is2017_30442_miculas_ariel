# Applying the MAFIA algorithm to text files
This a simple Perl script that parses text files, identifies sentences and then applies the MAFIA algorithm in order to extract the maximal frequent itemsets,
in this case the most frequent word associations in the input file.

For information about usage, run examples/freq.pl

For detailed information, see the documentation: report/ISreport.pdf