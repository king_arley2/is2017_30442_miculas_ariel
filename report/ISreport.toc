\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Tool setup}{2}{subsection.1.1}
\contentsline {paragraph}{How to install}{2}{section*.2}
\contentsline {paragraph}{How to run}{2}{section*.3}
\contentsline {subsection}{\numberline {1.2}Run existing samples}{2}{subsection.1.2}
\contentsline {section}{\numberline {2}Black box presentation}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Input data}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Output data}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Explain/justify results}{3}{subsection.2.3}
\contentsline {section}{\numberline {3}Project technical overview}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Topic}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Key concepts}{3}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Applicability}{4}{subsection.3.3}
\contentsline {section}{\numberline {4}Project details}{4}{section.4}
\contentsline {subsection}{\numberline {4.1}Methods/algorithms used}{4}{subsection.4.1}
\contentsline {paragraph}{Candidate Itemset Tree}{4}{section*.4}
\contentsline {paragraph}{Search space pruning}{4}{section*.5}
\contentsline {paragraph}{Vertical Bitmap Representation}{5}{section*.6}
\contentsline {subsection}{\numberline {4.2}How it works}{6}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Project specifics}{6}{subsection.4.3}
\contentsline {paragraph}{Comparison with DepthProject(Agarwal et al, KDD 2000)}{6}{section*.7}
\contentsline {section}{\numberline {5}Implementation details}{7}{section.5}
\contentsline {subsection}{\numberline {5.1}Outline code changes}{7}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Results}{7}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Test data}{7}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Compare results with other methods}{7}{subsubsection.5.2.2}
\contentsline {section}{\numberline {6}Related work}{8}{section.6}
\contentsline {subsection}{\numberline {6.1}Related concepts}{8}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Related methods}{8}{subsection.6.2}
\contentsline {section}{\numberline {7}Contribution}{9}{section.7}
\contentsline {subsection}{\numberline {7.1}Personal contribution}{9}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Practical applicability}{9}{subsection.7.2}
\contentsline {section}{\numberline {8}Conclusions}{9}{section.8}
\contentsline {section}{\numberline {9}References}{9}{section.9}
\contentsline {section}{\numberline {10}Appendix 1}{9}{section.10}
\contentsline {paragraph}{Pseudocode}{9}{section*.8}
